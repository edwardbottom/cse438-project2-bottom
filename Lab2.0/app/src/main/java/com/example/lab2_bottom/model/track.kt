package com.example.lab2_bottom.model

import java.io.Serializable
import kotlin.collections.ArrayList
import java.net.URL

class Track(): Serializable {

    // instance variable declarations
    private var name: String = ""
    private var artist: String = ""
    private var duration: Long = 0
    private var playCount: Long = 0
    private var listeners: Long = 0
    private var imageUrl: String = ""
    private var url: String =""


    //constructor declaration
    constructor(
        name: String,
        artist: String,
        duration: Long,
        playCount: Long,
        listeners: Long,
        imageUrl: String,
        url: String

    //set values in constructor
    ) : this() {
        this.name = name
        this.artist = artist
        this.duration = duration
        this.playCount = playCount
        this.listeners = listeners
        this.imageUrl = imageUrl
        this.url = url
    }

    fun getName(): String {
        return this.name
    }

    fun getArtist(): String {
        return this.artist
    }

    fun getListeners(): Long {
        return this.listeners
    }

    fun getDuration(): Long {
        return this.duration
    }

    fun getPlayCount(): Long {
        return this.playCount
    }

    fun getImageUrl(): String {
        return this.imageUrl
    }

    fun getUrl(): String {
        return this.url
    }
}