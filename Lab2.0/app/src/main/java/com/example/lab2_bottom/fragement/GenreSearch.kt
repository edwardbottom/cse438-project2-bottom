package com.example.lab2_bottom.fragement

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import com.example.cse438.studio3.db.FavoritesDatabaseHelper

import com.example.lab2_bottom.R
import kotlinx.android.synthetic.main.fragment_genre_search.*
import kotlinx.android.synthetic.main.fragment_home.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [GenreSearch.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [GenreSearch.newInstance] factory method to
 * create an instance of this fragment.
 *
 */

@SuppressLint("ValidFragment")
class GenreSearch constructor(context: Context): Fragment() {

    private var parentContext: Context = context
    private val db = FavoritesDatabaseHelper(context)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_genre_search, container, false)
    }

    override fun onStart(){
        super.onStart()

        //search feature
        search_text_genre.setOnEditorActionListener { _, actionId, _ ->
            //if the text is null, create toast
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val searchText = search_text_genre.text
                search_text_genre.setText("")
                if (searchText.toString() == "") {
                    val toast = Toast.makeText(this.parentContext, "Please enter text", Toast.LENGTH_SHORT)
                    toast.setGravity(Gravity.CENTER, 0, 0)
                    toast.show()
                    return@setOnEditorActionListener true
                }
                //else perform search
                else {

                    //store search text in history
                    db.addSearchToHistory(searchText.toString())
                    //display search text
                    search_text_display_genre.text = "Results For: " + searchText.toString() + " from Last.FM"

                    //load new fragment
                    val fm2 = fragmentManager
                    val arguements: Bundle = Bundle()
                    arguements.putString("search_value", searchText.toString())
                    val fragment = SearchTrackGenreResults(this.parentContext)
                    fragment.arguments = arguements
                    val ft2 = fm2?.beginTransaction()
                    ft2?.replace(R.id.list_holder_genre, fragment)
                    ft2?.commit()
                    return@setOnEditorActionListener false
                }
            }

            return@setOnEditorActionListener false
        }
    }


}
