package com.example.lab2_bottom.util

import android.text.TextUtils
import android.util.Log
import com.example.lab2_bottom.model.Track
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.nio.charset.Charset
import kotlin.collections.ArrayList

class QueryUtils {
    companion object {
        //sets urlls for requests
        private val LogTag = this::class.java.simpleName
        private const val BaseURL = "http://ws.audioscrobbler.com/2.0/?method=" // localhost URL
        private var searchURL = "http://ws.audioscrobbler.com/2.0/?method=artist.getTopTracks&artist="

        //searchs for a track by artist name
        fun fetchTrackData(jsonQueryString: String): ArrayList<Track>? {
            val url: URL? = createUrl(searchURL + jsonQueryString + "&api_key=d2f5b9d095a2b8ed30056897a0774cd8&format=json")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return extractDataFromJsonSearch(jsonResponse, jsonQueryString)
        }

        fun fetchTracksByTag(jsonQueryString: String): ArrayList<Track>?{
            val url: URL? = createUrl("http://ws.audioscrobbler.com/2.0/?method=tag.gettoptracks&tag=" + jsonQueryString + "&api_key=d2f5b9d095a2b8ed30056897a0774cd8&format=json")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return extractDataFromJsonSearchGenre(jsonResponse, jsonQueryString)
        }

        //gets the current top tracks
        fun fetchTopTacks() : ArrayList<Track>? {
            val url: URL? = createUrl("http://ws.audioscrobbler.com/2.0/?method=chart.gettoptracks&api_key=d2f5b9d095a2b8ed30056897a0774cd8&format=json")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }
            return extractDataFromJsonDefault(jsonResponse)
        }

        //creates the url for a request
        private fun createUrl(stringUrl: String): URL? {
            var url: URL? = null
            try {
                url = URL(stringUrl)
            }
            catch (e: MalformedURLException) {
                Log.e(this.LogTag, "Problem building the URL.", e)
            }

            return url
        }

        //ames the http request
        private fun makeHttpRequest(url: URL?): String {
            var jsonResponse = ""

            if (url == null) {
                return jsonResponse
            }

            var urlConnection: HttpURLConnection? = null
            var inputStream: InputStream? = null
            try {
                //success case
                urlConnection = url.openConnection() as HttpURLConnection
                urlConnection.readTimeout = 10000 // 10 seconds
                urlConnection.connectTimeout = 15000 // 15 seconds
                urlConnection.requestMethod = "GET"
                urlConnection.connect()

                if (urlConnection.responseCode == 200) {
                    inputStream = urlConnection.inputStream
                    jsonResponse = readFromStream(inputStream)
                    //println(jsonResponse)
                }
                else {
                    //failure case
                    Log.e(this.LogTag, "Error response code: ${urlConnection.responseCode}")
                }
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem retrieving the product data results: $url", e)
            }
            finally {
                urlConnection?.disconnect()
                inputStream?.close()
            }

            return jsonResponse
        }

        //read data from an input stream reader
        private fun readFromStream(inputStream: InputStream?): String {
            val output = StringBuilder()
            if (inputStream != null) {
                val inputStreamReader = InputStreamReader(inputStream, Charset.forName("UTF-8"))
                val reader = BufferedReader(inputStreamReader)
                var line = reader.readLine()
                while (line != null) {
                    output.append(line)
                    line = reader.readLine()
                }
            }
            return output.toString()
        }

        //gets data from a json object and returns a list or tracks
        private fun extractDataFromJsonDefault(productJson: String?): ArrayList<Track>? {
            //if null
            if (TextUtils.isEmpty(productJson)) {
                return null
            }

            val trackList = ArrayList<Track>()
            try {
                //parse into sub objects
                val jsonObj = JSONObject(productJson)
                val baseJsonResponse = returnValueOrDefault<JSONObject>(jsonObj, "tracks") as JSONObject
                val jsonArrayResponse = returnValueOrDefault<JSONArray>(baseJsonResponse, "track") as JSONArray
                //create a single track
                for (i in 0 until jsonArrayResponse.length()){
                    val productObject = jsonArrayResponse.getJSONObject(i)
                    val artistObj = returnValueOrDefault<JSONObject>(productObject, "artist") as JSONObject
                    val imageArray = returnValueOrDefault<JSONArray>(productObject, "image") as JSONArray
                    val imageObj = imageArray.getJSONObject(1)

                    //add the track to the list
                    trackList.add(Track(
                        returnValueOrDefault<String>(productObject, "name") as String,
                        returnValueOrDefault<String>(artistObj, "name") as String,
                        returnValueOrDefault<Long>(productObject, "duration") as Long,
                        returnValueOrDefault<Long>(productObject, "playcount") as Long,
                        returnValueOrDefault<Long>(productObject, "listeners") as Long,
                        returnValueOrDefault<String>(imageObj, "#text") as String,
                        returnValueOrDefault<String>(productObject, "url") as String
                    ))

                }
            }
            //failure case
            catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }
            //success case
            return trackList
        }

        //returns a list of tracks from a specified artist search
        private fun extractDataFromJsonSearch(productJson: String?, artist: String): ArrayList<Track>? {
            //if null
            if (TextUtils.isEmpty(productJson)) {
                return null
            }

            val trackList = ArrayList<Track>()
            try {
                //subdivide object
                val jsonObj = JSONObject(productJson)
                val baseJsonResponse = returnValueOrDefault<JSONObject>(jsonObj, "toptracks") as JSONObject
                val jsonArrayResponse = returnValueOrDefault<JSONArray>(baseJsonResponse, "track") as JSONArray
                for (i in 0 until jsonArrayResponse.length()){
                    val productObject = jsonArrayResponse.getJSONObject(i)
                    val artistObj = returnValueOrDefault<JSONObject>(productObject, "artist") as JSONObject
                    val imageArray = returnValueOrDefault<JSONArray>(productObject, "image") as JSONArray
                    val imageObj = imageArray.getJSONObject(1)

                    //store tracks in the list
                    trackList.add(Track(
                        returnValueOrDefault<String>(productObject, "name") as String,
                        returnValueOrDefault<String>(artistObj,"name") as String,
                        0,
                        returnValueOrDefault<Long>(productObject, "playCount") as Long,
                        returnValueOrDefault<Long>(productObject, "listeners") as Long,
                        returnValueOrDefault<String>(imageObj, "#text") as String,
                        returnValueOrDefault<String>(productObject, "url") as String
                    ))

                }
            }
            catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }
            return trackList
        }

        //extracts data from json object from genre query
        private fun extractDataFromJsonSearchGenre(productJson: String?, artist: String): ArrayList<Track>? {
            //if null
            if (TextUtils.isEmpty(productJson)) {
                return null
            }

            val trackList = ArrayList<Track>()
            try {
                //subdivide object
                val jsonObj = JSONObject(productJson)
                val baseJsonResponse = returnValueOrDefault<JSONObject>(jsonObj, "tracks") as JSONObject
                val jsonArrayResponse = returnValueOrDefault<JSONArray>(baseJsonResponse, "track") as JSONArray
                for (i in 0 until jsonArrayResponse.length()){
                    val productObject = jsonArrayResponse.getJSONObject(i)
                    val artistObj = returnValueOrDefault<JSONObject>(productObject, "artist") as JSONObject
                    val imageArray = returnValueOrDefault<JSONArray>(productObject, "image") as JSONArray
                    val imageObj = imageArray.getJSONObject(1)

                    //store tracks in the list
                    trackList.add(Track(
                        returnValueOrDefault<String>(productObject, "name") as String,
                        returnValueOrDefault<String>(artistObj,"name") as String,
                        returnValueOrDefault<Long>(productObject, "duration") as Long,
                        0,
                        0,
                        returnValueOrDefault<String>(imageObj, "#text") as String,
                        returnValueOrDefault<String>(productObject, "url") as String
                    ))

                }
            }
            catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }
            return trackList
        }

        //parses individual values in json objects and returns the correct type
        private inline fun <reified T> returnValueOrDefault(json: JSONObject, key: String): Any? {
            when (T::class) {
                String::class -> {
                    return if (json.has(key)) {
                        json.getString(key)
                    } else {
                        ""
                    }
                }
                Int::class -> {
                    return if (json.has(key)) {
                        json.getInt(key)
                    }
                    else {
                        return -1
                    }
                }
                Double::class -> {
                    return if (json.has(key)) {
                        json.getDouble(key)
                    }
                    else {
                        return -1.0
                    }
                }
                Long::class -> {
                    return if (json.has(key)) {
                        json.getLong(key)
                    }
                    else {
                        return (-1).toLong()
                    }
                }
                JSONObject::class -> {
                    return if (json.has(key)) {
                        json.getJSONObject(key)
                    }
                    else {
                        return null
                    }
                }
                JSONArray::class -> {
                    return if (json.has(key)) {
                        json.getJSONArray(key)
                    }
                    else {
                        return null
                    }
                }
                else -> {
                    return null
                }
            }
        }
    }
}