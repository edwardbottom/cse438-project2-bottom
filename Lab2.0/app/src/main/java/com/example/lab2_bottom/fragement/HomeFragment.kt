package com.example.lab2_bottom.fragement

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import com.example.cse438.studio3.db.FavoritesDatabaseHelper
import com.example.lab2_bottom.R
import com.example.lab2_bottom.viewmodel.TrackViewModel
import kotlinx.android.synthetic.main.fragment_genre_search.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_search_tracks.*





/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [HomeFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */

@SuppressLint("ValidFragment")
class HomeFragment  constructor(context: Context) : Fragment() {
    //instance variables
    private var parentContext: Context = context
    private var initialized: Boolean = false
    private val db = FavoritesDatabaseHelper(context)

    //creates the fragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(com.example.lab2_bottom.R.layout.fragment_home, container, false)
    }

    override fun onResume() {
        super.onResume()
        //intialize the fragment manager
            val fm = fragmentManager
            val ft = fm?.beginTransaction()
            ft?.add(R.id.list_holder, TopTracksFragment(this.parentContext))
            ft?.commit()

            //search feature
            search_text.setOnEditorActionListener { _, actionId, _ ->
                //if the text is null, create toast
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    val searchText = search_text.text
                    search_text.setText("")
                    if (searchText.toString() == "") {
                        val toast = Toast.makeText(this.parentContext, "Please enter text", Toast.LENGTH_SHORT)
                        toast.setGravity(Gravity.CENTER, 0, 0)
                        toast.show()
                        return@setOnEditorActionListener true
                    }
                    //else perform search
                    else {

                        //store search text in history
                        db.addSearchToHistory(searchText.toString())
                        //display search text
                        search_text_display.text = "Results For: " + searchText.toString() + " from Last.FM"

                        //load new fragment
                        val fm2 = fragmentManager
                        val arguements: Bundle = Bundle()
                        arguements.putString("search_value", searchText.toString())
                        val fragment = SearchTracksFragment(this.parentContext)
                        fragment.arguments = arguements
                        val ft2 = fm2?.beginTransaction()
                        ft2?.replace(com.example.lab2_bottom.R.id.list_holder, fragment)
                        ft2?.commit()
                        return@setOnEditorActionListener false
                    }
                }

                return@setOnEditorActionListener false
            }

            this.initialized = true
    }

    override fun onStart(){
        super.onStart()
        //intialize the fragment manager
        if (!this.initialized) {
            val fm = fragmentManager
            val ft = fm?.beginTransaction()
            ft?.add(R.id.list_holder, TopTracksFragment(this.parentContext))
            ft?.commit()

            //search feature
            search_text.setOnEditorActionListener { _, actionId, _ ->
                //if the text is null, create toast
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    val searchText = search_text.text
                    search_text.setText("")
                    if (searchText.toString() == "") {
                        val toast = Toast.makeText(this.parentContext, "Please enter text", Toast.LENGTH_SHORT)
                        toast.setGravity(Gravity.CENTER, 0, 0)
                        toast.show()
                        return@setOnEditorActionListener true
                    }
                    //else perform search
                    else {

                        //store search text in history
                        db.addSearchToHistory(searchText.toString())
                        //display search text
                        search_text_display.text = "Results For: " + searchText.toString() + " from Last.FM"

                        //load new fragment
                        val fm2 = fragmentManager
                        val arguements: Bundle = Bundle()
                        arguements.putString("search_value", searchText.toString())
                        val fragment = SearchTracksFragment(this.parentContext)
                        fragment.arguments = arguements
                        val ft2 = fm2?.beginTransaction()
                        ft2?.replace(com.example.lab2_bottom.R.id.list_holder, fragment)
                        ft2?.commit()
                        return@setOnEditorActionListener false
                    }
                }

                return@setOnEditorActionListener false
            }

            this.initialized = true
        }
    }


}
