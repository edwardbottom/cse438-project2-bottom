package com.example.lab2_bottom.fragement

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.example.lab2_bottom.R
import com.example.lab2_bottom.activity.TrackDetails
import com.example.lab2_bottom.model.Track
import com.example.lab2_bottom.viewmodel.TrackViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_top_tracks.*
import kotlinx.android.synthetic.main.top_track_view.view.*
import java.lang.Thread.sleep

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [TopTracksFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [TopTracksFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
@SuppressLint("ValidFragment")
class TopTracksFragment constructor(context: Context) : Fragment() {
    //instance variables
    private var parentContext: Context = context
    private lateinit var viewModel: TrackViewModel
    private var adapter = NewTrackAdapter()

    //track list for top tracks
    private var trackList: ArrayList<Track> = ArrayList()

    //creates fragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    //inflates the fragment
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_top_tracks, container, false)
    }

    override fun onResume(){
        super.onResume()
        if(viewModel.getTopTracks().value != null){
            trackList = viewModel.getTopTracks().value!!
            }
        println("on resume!!!")
        }

    override fun onStart(){
        super.onStart()
        //gets the view model
        viewModel = ViewModelProviders.of(this).get(TrackViewModel::class.java)
        grid_view_top_tracks.layoutManager = GridLayoutManager(parentContext, 2)

        //observer to track data in the view model
        val observer = Observer<ArrayList<Track>> {
            grid_view_top_tracks.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return trackList[p0].getName() == trackList[p1].getName()
                }

                override fun getOldListSize(): Int {
                    return trackList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return trackList[p0] == trackList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            trackList = it ?: ArrayList()
        }

        //sets the view model to the observer
        viewModel.getTopTracks().observe(this, observer)
    }

    //adapter class for the top tracks
    inner class NewTrackAdapter: RecyclerView.Adapter<NewTrackAdapter.NewTrackViewHolder>() {

        //creates the view holder
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): NewTrackViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.top_track_view, p0, false)
            return NewTrackViewHolder(itemView)
        }

        //binds data for the view holder
        override fun onBindViewHolder(p0: NewTrackViewHolder, p1: Int) {
            val track = trackList!![p1]
            val imageURL = track.getImageUrl()
            if (imageURL == null) {
                // Do nothing for now
            }
            else {
                Picasso.with(this@TopTracksFragment.parentContext).load(track.getImageUrl()).into(p0.trackImg)
                Log.e("text", "image was set " + track.getImageUrl())
            }
            p0.trackTitle.text = track.getName()

            //sets listener for track avitivty
            p0.row.setOnClickListener {
                val intent = Intent(this@TopTracksFragment.parentContext, TrackDetails::class.java)
                intent.putExtra("name", track.getName())
                intent.putExtra("listeners", track.getListeners())
                intent.putExtra("playCount", track.getPlayCount())
                intent.putExtra("artist", track.getArtist())
                intent.putExtra("duration", track.getDuration())
                intent.putExtra("imageUrl", track.getImageUrl())
                intent.putExtra("url", track.getUrl())
                startActivity(intent)
            }
        }

        //gets the size of the track list
        override fun getItemCount(): Int {
            return trackList.size
        }

        //interface for xml
        inner class NewTrackViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val row = itemView

            var trackImg: ImageView = itemView.top_track_image
            var trackTitle: TextView = itemView.top_track_name
        }
    }
}