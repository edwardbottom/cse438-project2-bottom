package com.example.lab2_bottom.fragement

import android.annotation.SuppressLint
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.example.lab2_bottom.R
import com.example.lab2_bottom.activity.TrackDetails
import com.example.lab2_bottom.model.Track
import com.example.lab2_bottom.viewmodel.TrackViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_search_track_genre_results.*
import kotlinx.android.synthetic.main.fragment_search_tracks.*
import kotlinx.android.synthetic.main.top_track_view.view.*


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SearchTrackGenreResults.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SearchTrackGenreResults.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
@SuppressLint("ValidFragment")
class SearchTrackGenreResults constructor(context: Context) : Fragment() {
    //instance variables
    private var searchText : String = ""
    private var parentContext : Context = context
    private lateinit var viewModel: TrackViewModel
    private var adapter = NewGenreAdapter()

    private var trackList: ArrayList<Track> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_track_genre_results, container, false)
    }

    override fun onStart(){
        super.onStart()
        //bundle to transfer data
        var bundle = this.arguments

        // Inflate the layout for this fragment
        searchText = bundle?.getString("search_value").toString()
        viewModel = ViewModelProviders.of(this).get(TrackViewModel::class.java)
        grid_view_search_tracks_genre.layoutManager = GridLayoutManager(parentContext, 2)

        //observer object for searched tracks query
        val observer = Observer<ArrayList<Track>> {
            grid_view_search_tracks_genre.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return trackList[p0].getName() == trackList[p1].getName()
                }

                override fun getOldListSize(): Int {
                    return trackList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return trackList[p0] == trackList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            trackList = it ?: ArrayList()
        }
        //sets view model to oberser
        viewModel.getTrackByTag(searchText).observe(this, observer)
    }

    //creates the view
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    //new track apater class
    inner class NewGenreAdapter: RecyclerView.Adapter<NewGenreAdapter.NewTrackViewHolder>() {

        //creates the view holder
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): NewTrackViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.top_track_view, p0, false)
            return NewTrackViewHolder(itemView)
        }

        //binds all track values to the view holder
        override fun onBindViewHolder(p0: NewTrackViewHolder, p1: Int) {
            //setting variables
            val track = trackList[p1]

            //setting image
            val imageURL = track.getImageUrl()
            if (imageURL == null) {
                // Do nothing for now
            }
            else {
                Picasso.with(this@SearchTrackGenreResults.parentContext).load(track.getImageUrl()).into(p0.trackImg)
            }
            p0.trackTitle.text = track.getName()

            //setting listener to view track details
            p0.row.setOnClickListener {
                val intent = Intent(this@SearchTrackGenreResults.parentContext, TrackDetails::class.java)
                intent.putExtra("name", track.getName())
                intent.putExtra("listeners", track.getListeners())
                intent.putExtra("playCount", track.getPlayCount())
                intent.putExtra("artist", track.getArtist())
                intent.putExtra("duration", track.getDuration())
                intent.putExtra("imageUrl", track.getImageUrl())
                intent.putExtra("url", track.getUrl())
                startActivity(intent)
            }
        }

        //get the size of the track list
        override fun getItemCount(): Int {
            return trackList.size
        }

        //interface for xml
        inner class NewTrackViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val row = itemView

            var trackImg: ImageView = itemView.top_track_image
            var trackTitle: TextView = itemView.top_track_name
        }
    }

}
