package com.example.lab2_bottom.activity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.util.Log
import com.example.cse438.studio3.db.FavoritesDatabaseHelper
import com.example.lab2_bottom.R
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.activity_track_details.*
import kotlinx.android.synthetic.main.content_track_details.*

class TrackDetails : AppCompatActivity() {
    private lateinit var artist: String
    private lateinit var name: String
    private var listeners: Long = 0
    private var playCount : Long = 0
    private var duration : Long = 0
    private lateinit var imageUrl: String
    private var url : String = ""
    private val db = FavoritesDatabaseHelper(this!!)
    //private var isInPlaylist = false

    override fun onCreate(savedInstanceState: Bundle?) {
        //creates and initializes the fragement
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_details)
        setSupportActionBar(toolbar)

        //sets values from main activity
        artist = intent.getStringExtra("artist")
        name = intent.getStringExtra("name")
        listeners = intent.getLongExtra("listeners", 0)
        duration = intent.getLongExtra("duration", 0)
        imageUrl = intent.getStringExtra("imageUrl")
        url = intent.getStringExtra("url")

        //sets text in xml
        trackName.text = "Song: " + name.toString()
        trackArtist.text = "Artist: " + artist.toString()
        println(db.containsName(name).toString() + "is contains name funciton value")
        //sets button text
        if(db.containsName(name)){
            add_to_playlist_button.text = "Remove From Playlist"
        }
        else{
            add_to_playlist_button.text = "Add to Playlist"
        }

        val compVal : Long = 0
        if(duration == compVal){
            trackDuration.text = "Duration: n/a"
        }
        else{
            trackDuration.text = "Duration: " + duration.toString()
        }
        trackListeners.text = "Listeners: " + listeners.toString()
        trackUrl.text = url

        //sets image in xml
        if (imageUrl == null) {
            // Do nothing for now
        }
        else {
            Picasso.with(this).load(imageUrl).into(content_track_image)
        }


        //returns to main activity
        return_home_button.setOnClickListener(){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        //adds or removes an item from a playlist
        add_to_playlist_button.setOnClickListener(){
            //add to playlist
            println("BUTTON CLICKED")
            if(!db.containsName(name)){
                db.addTrackToPlaylist(name.toString(), artist.toString(), duration.toLong(), listeners.toLong(), playCount.toLong(), imageUrl.toString())
                println("contains name was false and value was ADDED to the database")
            }
            //remove from playlist
            else{
                db.removeTrackFromPlaylist(name.toString(), artist.toString())
                println("conatins name was true and value was REMOVED from the database")
            }

            //update status in playlist variable
            if(db.containsName(name)){
                add_to_playlist_button.text = "Remove From Playlist"
            }
            else{
                add_to_playlist_button.text = "Add to Playlist"
            }
        }
    }

}
