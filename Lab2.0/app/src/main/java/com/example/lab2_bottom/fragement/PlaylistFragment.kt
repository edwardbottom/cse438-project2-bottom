package com.example.lab2_bottom.fragement

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.example.lab2_bottom.R
import com.example.lab2_bottom.model.Track
import com.example.lab2_bottom.viewmodel.TrackViewModel
import kotlinx.android.synthetic.main.fragment_playlist.*
import kotlinx.android.synthetic.main.playlist_item.view.*


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PlaylistFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [PlaylistFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */

@SuppressLint("ValidFragment")
class PlaylistFragment constructor(context: Context) : Fragment() {
    //instance variables
    private var adapter = PlaylistAdapter()
    private lateinit var viewModel: TrackViewModel

    private var playList: ArrayList<Track> = ArrayList()

    //creates the fragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    //creates the fragment view
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_playlist, container, false)
    }

    override fun onStart() {
        super.onStart()

        //intializes the list for view
        playlist_list.layoutManager = LinearLayoutManager(this.context)
        playlist_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(TrackViewModel::class.java)

        //observer to track changes in the virtual model
        val observer = Observer<ArrayList<Track>> {
            playlist_list.adapter = adapter
            //checks to see if the tracks are the same or not and if they are not tells the list that it needs to update
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    if(p0 >= playList.size || p1 >= playList.size) {
                        return false;
                    }
                    return playList[p0].getName() == playList[p1].getName()
                }

                //gets the old size of the playlist
                override fun getOldListSize(): Int {
                    return playList.size
                }

                //gets the new size of the playlist
                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                //returns true if two values are the same
                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return playList[p0] == playList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            playList = it ?: ArrayList()
        }
        //sets the observer to the view modles playlist
        viewModel.getPlaylist().observe(this, observer)
    }

    //apater class for the playlist
    inner class PlaylistAdapter: RecyclerView.Adapter<PlaylistAdapter.PlaylistViewHolder>() {

        //view holder for adapter
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PlaylistViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.playlist_item, p0, false)
            return PlaylistViewHolder(itemView)
        }

        //sets xml values
        override fun onBindViewHolder(p0: PlaylistViewHolder, p1: Int) {
            val track = playList[p1]
            p0.name.text = "Song Name: " + track.getName()
            p0.artist.text = "Song Artist: " + track.getArtist()
        }

        //returns size of playlist
        override fun getItemCount(): Int {
            return playList.size
        }

        //interface for xml
        inner class PlaylistViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            var name: TextView = itemView.playlist_track_name
            var artist: TextView = itemView.playlist_artist_name
        }
    }

}
