package com.example.lab2_bottom.fragement

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.example.lab2_bottom.R
import com.example.lab2_bottom.model.Track
import com.example.lab2_bottom.viewmodel.TrackViewModel
import kotlinx.android.synthetic.main.fragment_history.*
import kotlinx.android.synthetic.main.fragment_playlist.*
import kotlinx.android.synthetic.main.history_item.view.*
import kotlinx.android.synthetic.main.playlist_item.view.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [History.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [History.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
@SuppressLint("ValidFragment")
class History constructor(context: Context) : Fragment() {

    private var parentContext: Context = context
    private var adapter = HistoryAdapter()
    private lateinit var viewModel: TrackViewModel

    private var historyList: ArrayList<String>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onResume(){
        super.onResume()
        println("onresume is runnning!!!!!!!!!!!!!!!!!!!!!!!!!")
        historyList = viewModel.getHistory().value
    }

    override fun onStart() {
        super.onStart()

        println("here")

        //intializes the list for view
        history_list.layoutManager = LinearLayoutManager(this.context)
        history_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(TrackViewModel::class.java)

        //observer to track changes in the virtual model
        val observer = Observer<ArrayList<String>> {
            history_list.adapter = adapter
            //checks to see if the tracks are the same or not and if they are not tells the list that it needs to update
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    if(p0 >= historyList!!.size || p1 >= historyList!!.size) {
                        return false;
                    }
                    return historyList!![p0] == historyList!![p1]
                }

                //gets the old size of the playlist
                override fun getOldListSize(): Int {
                    return historyList!!.size
                }

                //gets the new size of the playlist
                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                //returns true if two values are the same
                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return historyList!![p0] == historyList!![p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            historyList = it ?: ArrayList()
        }
        //sets the observer to the view modles playlist
        viewModel.getHistory().observe(this, observer)
    }

    //apater class for the playlist
    inner class HistoryAdapter: RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>() {

        //view holder for adapter
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HistoryViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.history_item, p0, false)
            return HistoryViewHolder(itemView)
        }

        //sets xml values
        override fun onBindViewHolder(p0: HistoryViewHolder, p1: Int) {
            val search = historyList!![p1]
            p0.search.text = search
        }

        //returns size of playlist
        override fun getItemCount(): Int {
            return historyList!!.size
        }

        //interface for xml
        inner class HistoryViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            var search: TextView = itemView.history_list_item
        }
    }
}
