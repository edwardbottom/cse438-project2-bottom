package com.example.cse438.studio3.db

import android.provider.BaseColumns

class DbSettings {
    //database object
    companion object {
        const val DB_NAME = "playlist.db"
        const val DB_VERSION = 5
    }

    //playlist table
    class DBPlaylistEntry: BaseColumns {
        companion object {
            const val TABLE = "playlist"
            const val ID = BaseColumns._ID
            const val COL_NAME = "name"
            const val COL_ARTIST = "artist"
            const val COL_DURATION = "duration"
            const val COL_PLAYCOUNT = "playcount"
            const val COL_LISTENERS = "listeners"
            const val COL_IMAGEURL = "imageUrl"
        }
    }

    //playlist table
    class DBHistoryEntry: BaseColumns {
        companion object {
            const val TABLE = "history"
            const val ID = BaseColumns._ID
            const val COL_ARTIST = "artist"
        }
    }

}