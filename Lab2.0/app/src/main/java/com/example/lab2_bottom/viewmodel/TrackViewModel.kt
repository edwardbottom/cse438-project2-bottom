package com.example.lab2_bottom.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.os.AsyncTask
import android.provider.BaseColumns
import android.util.Log
import com.example.cse438.studio3.db.DbSettings
import com.example.cse438.studio3.db.FavoritesDatabaseHelper
import com.example.lab2_bottom.model.Track
import com.example.lab2_bottom.util.QueryUtils


class TrackViewModel (application: Application) : AndroidViewModel(application){
    //set instance variables and data stores
    private var _trackList: MutableLiveData<ArrayList<Track>> = MutableLiveData()
    private var _playlistDBHelper: FavoritesDatabaseHelper = FavoritesDatabaseHelper(application)
    private var _playList: MutableLiveData<ArrayList<Track>> = MutableLiveData()
    private var _history: MutableLiveData<ArrayList<String>> = MutableLiveData()

    //gets tracks from load tracks
    fun getTracks(query: String): MutableLiveData<ArrayList<Track>>{
        loadTrack(query)
        return _trackList
    }

    //returns the top tracks list
    fun getTopTracks(): MutableLiveData<ArrayList<Track>>{
        loadTopTracks()
        return _trackList
    }

    //queries for tracks by a tag
    fun getTrackByTag(query: String): MutableLiveData<ArrayList<Track>>{
        loadTracksByTag(query)
        return _trackList
    }

    //asych cal to load artist tracks
    private fun loadTrack(query: String){
        ProductAsyncTask().execute(query)
    }

    //asych call to top tracks request
    private fun loadTopTracks(){
        SecondAsyncTask().execute()
    }

    private fun loadTracksByTag(query: String){
        TagsAsyncTask().execute(query)
    }

    //asych call to the top tracks
    @SuppressLint("StaticFieldLeak")
    inner class ProductAsyncTask: AsyncTask<String, Unit, ArrayList<Track>>() {
        //aschy call
        override fun doInBackground(vararg params: String?): ArrayList<Track>? {
            return QueryUtils.fetchTrackData(params[0]!!)
        }

        //execute the call
        override fun onPostExecute(result: ArrayList<Track>?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            }
            else {
                Log.e("RESULTS", result.toString())
                _trackList.value = result
            }
        }
    }

    //asych call to the artist search
    @SuppressLint("StaticFieldLeak")
    inner class SecondAsyncTask: AsyncTask<String, Unit, ArrayList<Track>>() {
        override fun doInBackground(vararg params: String?): ArrayList<Track>? {
            return QueryUtils.fetchTopTacks()
        }

        //executes a post request
        override fun onPostExecute(result: ArrayList<Track>?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            }
            else {
                _trackList.value = result
                var dummy = _trackList.value
            }
        }
    }

    //asych call to the artist search
    @SuppressLint("StaticFieldLeak")
    inner class TagsAsyncTask: AsyncTask<String, Unit, ArrayList<Track>>() {
        override fun doInBackground(vararg params: String?): ArrayList<Track>? {
            return QueryUtils.fetchTracksByTag(params[0]!!)
        }

        //executes a post request
        override fun onPostExecute(result: ArrayList<Track>?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            }
            else {
                _trackList.value = result
                var dummy = _trackList.value
            }
        }
    }

    //returns the playlist from the database
    fun getPlaylist(): MutableLiveData<ArrayList<Track>> {
        loadPlaylist()
        return _playList
    }

    //queries the database for the playlist
    fun loadPlaylist() {
        //instance of the database
        val db = _playlistDBHelper.readableDatabase

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        val projection = arrayOf(DbSettings.DBPlaylistEntry.COL_NAME, DbSettings.DBPlaylistEntry.COL_ARTIST)

        //keeps track of where we are in our results set by iterating through given points in the database
        val cursor = db.query(
            DbSettings.DBPlaylistEntry.TABLE,   // The table to query
            projection,             // The array of columns to return (pass null to get all)
            null,              // The columns for the WHERE clause
            null,          // The values for the WHERE clause
            null,                   // don't group the rows
            null,                   // don't filter by row groups
            null               // The sort order
        )

        var playlist = ArrayList<Track>()
        //iterate through the cursor using a move to next
        with(cursor) {
            while (moveToNext()) {
                val name = getString(getColumnIndexOrThrow(DbSettings.DBPlaylistEntry.COL_NAME))
                val artist = getString(getColumnIndexOrThrow(DbSettings.DBPlaylistEntry.COL_ARTIST))
                val t = Track(name, artist, 0, 0, 0, "", "")
                playlist.add(t)
            }
        }
        playlist.reverse()
        _playList.value = playlist
    }

    //returns the list of histories
    fun getHistory(): MutableLiveData<ArrayList<String>>{
        loadHistory()
        return _history
    }

    //querys the database for search history
    fun loadHistory() {
        //instance of the database
        val db = _playlistDBHelper.readableDatabase

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        val projection = arrayOf(DbSettings.DBHistoryEntry.COL_ARTIST)

        //keeps track of where we are in our results set by iterating through given points in the database
        val cursor = db.query(
            DbSettings.DBHistoryEntry.TABLE,   // The table to query
            projection,             // The array of columns to return (pass null to get all)
            null,              // The columns for the WHERE clause
            null,          // The values for the WHERE clause
            null,                   // don't group the rows
            null,                   // don't filter by row groups
            null               // The sort order
        )

        var history = ArrayList<String>()
        //iterate through the cursor using a move to next
        with(cursor) {
            while (moveToNext()) {
                history.add(getString(getColumnIndexOrThrow(DbSettings.DBPlaylistEntry.COL_ARTIST)))
            }
        }
        _history.value = history
    }
}