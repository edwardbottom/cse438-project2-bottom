package com.example.cse438.studio3.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import android.util.Log
import com.example.lab2_bottom.model.Track
//import jdk.nashorn.internal.objects.NativeDate.setYear



class FavoritesDatabaseHelper(context: Context): SQLiteOpenHelper(context, DbSettings.DB_NAME, null, DbSettings.DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        //creates the playlist table
        val createPlaylistTableQuery = "CREATE TABLE IF NOT EXISTS " + DbSettings.DBPlaylistEntry.TABLE + " ( " +
                DbSettings.DBPlaylistEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBPlaylistEntry.COL_NAME + " TEXT, " +
                DbSettings.DBPlaylistEntry.COL_ARTIST+ " TEXT, " +
                DbSettings.DBPlaylistEntry.COL_DURATION + " INTEGER, " +
                DbSettings.DBPlaylistEntry.COL_PLAYCOUNT + " INTEGER, " +
                DbSettings.DBPlaylistEntry.COL_LISTENERS + " INTEGER, "+
                DbSettings.DBPlaylistEntry.COL_IMAGEURL + " TEXT )"
        db?.execSQL(createPlaylistTableQuery)

        //creates the history table
        val createHistoryTableQuery = "CREATE TABLE IF NOT EXISTS " + DbSettings.DBHistoryEntry.TABLE + " ( " +
                DbSettings.DBHistoryEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBHistoryEntry.COL_ARTIST + " TEXT )"
        db?.execSQL(createHistoryTableQuery)
    }

    //updates and clears the database
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + DbSettings.DBPlaylistEntry.TABLE)
        onCreate(db)
    }

    //function to insert values into the database
    fun addTrackToPlaylist(name: String, artist: String, duration: Long, listeners: Long, playCount: Long, imageUrl: String) {
        // Gets the data repository in write mode
        val db = this.writableDatabase

        // Create a new map of values, where column names are the keys
        //intialize valeus to insert
        val values = ContentValues().apply {
            put(DbSettings.DBPlaylistEntry.COL_NAME, name)
            put(DbSettings.DBPlaylistEntry.COL_ARTIST, artist)
            put(DbSettings.DBPlaylistEntry.COL_DURATION, duration)
            put(DbSettings.DBPlaylistEntry.COL_LISTENERS, listeners)
            put(DbSettings.DBPlaylistEntry.COL_PLAYCOUNT, playCount)
            put(DbSettings.DBPlaylistEntry.COL_IMAGEURL, imageUrl)
        }

        val newRowId = db?.insert(DbSettings.DBPlaylistEntry.TABLE, null, values)
    }

    //removes the track from the playlist
    fun removeTrackFromPlaylist(name: String, artist: String){
        val db = this.writableDatabase

        //delete command
        db.delete(
            DbSettings.DBPlaylistEntry.TABLE,
            "${DbSettings.DBPlaylistEntry.COL_NAME}=?",
            arrayOf(name)
        )

        println("REMOVE TRACK RAN!!!!")
        db.close()
    }

    //returns true if the name is in the database
    fun containsName(name : String): Boolean {
        val selectQuery = "SELECT * FROM " + DbSettings.DBPlaylistEntry.TABLE.toString() + " WHERE " + DbSettings.DBPlaylistEntry.COL_NAME.toString() + "=?;"
        val db = this.readableDatabase
        val cursor = db.rawQuery(selectQuery, arrayOf(name))

        var counter = 0
        //if TABLE has rows
        if (cursor.moveToFirst()) {
            //Loop through the table rows
            do {
                counter = counter + 1
            } while (cursor.moveToNext())
        }
        db.close()
        if(counter > 0){
            return true
        }
        else{
            return false
        }
    }

    fun addSearchToHistory(search: String){
        // Gets the data repository in write mode
        val db = this.writableDatabase

        // Create a new map of values, where column names are the keys
        //intialize valeus to insert
        val values = ContentValues().apply {
            put(DbSettings.DBHistoryEntry.COL_ARTIST, search)
        }
        val newRowId = db?.insert(DbSettings.DBHistoryEntry.TABLE, null, values)
    }
}