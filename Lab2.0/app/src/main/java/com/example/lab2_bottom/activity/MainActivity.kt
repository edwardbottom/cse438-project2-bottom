package com.example.lab2_bottom.activity

import android.app.PendingIntent.getActivity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.WindowManager
import com.example.cse438.studio3.db.FavoritesDatabaseHelper
import com.example.lab2_bottom.R
import com.example.lab2_bottom.fragement.GenreSearch
import com.example.lab2_bottom.fragement.History
import com.example.lab2_bottom.fragement.HomeFragment
import com.example.lab2_bottom.fragement.PlaylistFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        //intialize activity and fragment
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fragmentAdapter = MyPagerAdapter(supportFragmentManager)
        viewpager_main.adapter = fragmentAdapter
        //viewpager_main.setOffscreenPageLimit(5);
        //sets up the view pager using tabs
        tabs_main.setupWithViewPager(viewpager_main)

        this.getWindow().setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

    }

    //adapter for tabs
    inner class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        //sets up the tab layout
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    HomeFragment(this@MainActivity)
                }
                1 -> {
                    PlaylistFragment(this@MainActivity)
                }
                2 ->{
                    GenreSearch(this@MainActivity)
                }
                else -> {
                    History(this@MainActivity)
                }
            }
        }

        //returns the number of tabs
        override fun getCount(): Int {
            return 4
        }

        //returns the title page
        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> "Home Page"
                1 -> "Playlist"
                2-> "Search Genre"
                else-> {
                    "View History"
                }
            }
        }
    }
}
