This app was made using a pixle 2 emulator for testing. Thus, when running for best performance and scaling, the same emulator should be used. 

When the user opens the app, they will find themselves at the home screen of the app, which will default to rendering the current top tracks
(at the moment, this is alot of Arriana Grande tracks). The user will also have a search bar at the top of the sceen, which if clicked on
will allow the user to search of music by the name of the artist. The results are shown in a 2 column grid view with images from the 
Fast.FM api and the name of the song displayed on each card. 

If a card is clicked on, the user will be taken to a new activity, where they will see information such as a url to the Last.fm website,
the name of the song, artist, and number of listeners. Occasionally an n/a or 0 will show up for a value. This happends when the api gives a value of 0
for an existing field and occasionally happens on requests and cannot really be prevented without either not showing the data or displaying a default.
They will also have the option to add and remove the selected track from the playlist by pressing the add to playlist button. If the user presses 
the cancel button, they will be taken back to the home page. 

The second tab shows the users playlist that they can customize when they select an item. This playlist renders all of the tracks in the 
playlist, displaying the tracks name and artist in a list view. 

The third tab, the seach by genre tab is half of my ceative portion. This page allows users to search for tracks by tags (or in this case 
genres like rock, pop, punk, etc.) and view in the same grid view as the home page. They also have the opportunity to look at the track
in detail and add it to the playlist by clicking on the track. This features allow for users to track tags and genres of music in addition to 
artists and see potentially new music or music they would not normally listen to and add it to the playlist. 

The fourth tab is a list view of all the user's search history. It tracks the user's searches in both the genre and home tab search and 
renders them in a list view. I thought of this feature becuase I often have trouble remembering my previous searches in apps and figured a
small reminder could be useful. I also had some gaps in my understanding of SQLite that this part of the creative portion helped me fix.

(10 / 10 points) The app displays the current top tracks in a GridView on startup
(10 / 10 points) The app uses a tab bar with two tabs, one for searching for tracks and one for looking at the playlist
(10 / 10 points) Data is pulled from the API and processed into a GridView on the main page. Makes use of a Fragment to display the results seamlessly.
(15 / 15 points) Selecting a track from the GridView opens a new activity with the track cover, title, and 3 other pieces of information as well as the ability to save it to the playlist.
(5 / 5 points) User can change search query by editing text field.
(10 / 10 points) User can save a track to their playlist, and the track is saved into a SQLite database.
(5 / 5 points) User can delete a track from the playlist (deleting it from the SQLite database itself).
(4 / 4 points) App is visually appealing
(1/ 1 point) Properly attribute Last.fm API as source of data.
(5 / 5 points) Code is well formatted and commented.
(10 / 10 points) All API calls are done asynchronously and do not stall the application.
(15 / 15 points) Creative portion: Be creative!

Total: 100 / 100

You nailed it! I love the genre and search history features. Fantastic job!